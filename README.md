# Cocktail Remastered - Utils

- [Cocktail Remastered - Utils](#cocktail-remastered---utils)
  - [1. Environnement](#1-environnement)
  - [2. Development](#2-development)
    - [2.1. Prerequisites](#21-prerequisites)
    - [2.2. Dev Containers](#22-dev-containers)
    - [2.3. Adding Database Data](#23-adding-database-data)
    - [2.4. Launch in development mode](#24-launch-in-development-mode)

## 1. Environnement

This project was made on a **Windows** computer. Some provided scripts might not work on other environnement. It will be up to you to debug and transcript if you are on any other OS.

This code base is in relation with the other parts of the Cocktail Remastered project.
For more convenience here is the recommended order to install the folders :

- Utils
- Backend
- Front

## 2. Development

### 2.1. Prerequisites

The following software must be installed:

- [Docker](https://docs.docker.com/engine/install/)
- [Node LTS 16+](https://nodejs.org/en/download/)

### 2.2. Dev Containers

The applications need Docker containers to work.
To start them (from this directory):

```bash
docker-compose up -d
```

The containers are:

- datagate-postgres (the database server)

  - The connection data are:
    - host: localhost
    - port: 5432
  - The user is:
    - postgres / postgres

To stop the containers:

```bash
docker-compose down
```

To stop the containers and delete the databases:

```bash
docker-compose down -v
```

### 2.3. Adding Database Data

In order to run the backend you will need to populate the database.

- Copy a recent dump of the database in this repo and rename it `database.dump`. You can always use the default provided dump
- Run the following command.

⚠️ Make sure you have your postgres container running !

WINDOWS :

```ps1
.\restore-db.ps1 postgres database.dump
```

### 2.4. Launch in development mode

To start the application components (from their respective directories):

- `backend`:

  ```bash
  cd backend && yarn install
  yarn start:dev
  ```
