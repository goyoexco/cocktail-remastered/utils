If ($args.Length -lt 2 -or $args[0] -eq "" -or -not (Test-Path -Path $args[1] -PathType Leaf)) {
    Write-Output "Usage: ./restore-db.ps1 database filePath.dump"
    Exit 1
}
$DATABASE = $args[0]
$PATH = $args[1]

docker cp $PATH  cocktail-remastered-postgres:/tmp/database.dump
if ($LastExitCode -eq 0) {
    docker exec  cocktail-remastered-postgres /bin/bash -c "pg_restore --no-owner --role=$DATABASE --username=$DATABASE --dbname=$DATABASE --clean --if-exists --verbose /tmp/database.dump"
}
